#
# Module for slapd proxy configuration
#
# Usage example;
#     slapd_proxy::config{ $fqdn:
#         bindas => 'smtp',
#         type   => 'host', 
#     }
#
# Configuration for different applications
define slapd_proxy::config(
   $ensure     = 'present',
   $runasuser  = 'openldap',
   $runasgroup = 'openldap',
   $type       = 'service',
   $sizelimit  = '25',
   $ldaphost   = 'ldap.stanford.edu',
   $debuglevel = '256',
   $keytabpath = NOPATH,
   $bindas
) {

    case $ensure {
        absent: { 
                 # Place holder
        }
        present: {
            # The slapd will run as this user and this group
            user { $runasuser: 
                groups => $runasgroup;
            }

            # Service principal for GSSAPI authentication
            if ($type == 'host' ) {
                $keytab = "$bindas/$fqdn"
            } elsif $type == 'service' {
                $keytab = "service/$bindas"
            } else {
                fail('Binding principal type should be either host or service')
            }

            if $keytabpath == 'NOPATH' {
                $keytabloc = "/etc/${bindas}.keytab"
            } else {
                $keytabloc = "$keytabpath"
            }

            base::wallet { "$keytab":
                path    => $keytabloc,
                owner   => 'root',
                group   => $runasgroup,
                mode    => 640,
                ensure  => present,
                require => Package['slapd'];
            }

            base::daemontools::supervise { 'slapd-k5':
                ensure  => present,
                content => template('slapd_proxy/slapd-k5.run.erb'),
                require => File[ $keytabloc ];
            }

            file { '/etc/default/slapd':
                content => template('slapd_proxy/slapd.erb'),
            }

            file { '/etc/ldap/conf.d/slapd_custom.conf':
                content => template('slapd_proxy/slapd_custom.conf.erb'),
            }
        }
        default: { crit "Invalid ensure value: $ensure" }
    }
}