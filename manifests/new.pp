# LDAP proxy configuration for access the central LDAP service.  The
# proxy makes a kerberos bind to the central server and local
# applications can bind anonymously.
#
# The default Kerberos principal used to access the LDAP directory is
# the host keytab stored in /etc/krb5.conf.  If any other principal is
# specified the keytab will be downloaded using wallet by this
# procedure.
#
# This define takes care of downloading the correct packages,
# installing the ldap configuration, downloading the keytab, and
# setting up the supervise to keep the keytab refreshed.  What is
# required to use the service is to get dataowner approval to access
# the directory, and have the ACLs on the central LDAP service
# modified to allow the access.
#
# Example:
#
#  slapd_proxy { ensure => present }
#

define slapd_proxy::new (
  $ensure       = 'present',
  $debuglevel   = 'stats',
  $sizelimit    = 'unlimited',
  $principal    = 'HOST',
  $ldapHost     = 'ldap.stanford.edu',
  $ldapConfBase = 'dc=stanford,dc=edu',
  $ldapUser     = 'ldap',
  $ldapGroup    = 'ldap',
  $keytab       = 'slapd-proxy.keytab'
) {

  # Set variables first so that we can process ensure => absent.
  case $operatingsystem {
    'RedHat': { $ldapPath = '/etc/openldap' }
    default:  { $ldapPath = '/etc/ldap'     }
  }

  case $ensure {
    present: {
      case $operatingsystem {
        'RedHat': { include slapd_proxy::packages::redhat }
        default:  { include slapd_proxy::packages::debian }
      }
      file {
        "$ldapPath":
          mode   => 755,
          ensure => directory;
        "$ldapPath/ldap.conf":
          mode    => 644,
          content => template('slapd_proxy/etc/ldap.conf.erb');
        "$ldapPath/schema":
          source  => 'puppet:///s_ldap/schema/prod24',
          recurse => true;
        "$ldapPath/slapd.d":
          ensure  => absent,
          recurse => true,
          force   => true;
        "$ldapPath/slapd.conf": 
          content => template('slapd_proxy/slapd-proxy.conf.erb');
      }
      case $principal {
        'HOST': {
          $keytabFile = '/etc/krb5.keytab'
        }
        default: {
          $keytabFile = "$ldapPath/$keytab"
          base::wallet { "$principal":
            owner   => "$ldapUser",
            group   => "$ldapGroup",
            path    => "$keytabFile",
            ensure  => present,
          }
        }
      }
      base::daemontools::supervise { 'slapd-proxy':
        ensure => present,
        content => template('slapd_proxy/service/slapd-proxy.run.erb'),
      }
    }
    absent: {
      base::daemontools::supervise { 'slapd-proxy': ensure => absent }
    }
    default: {
      fail('Invalid ensure specified for slapd_proxy')
    }
  }
}

# ------------------------------------------------------------
# Install the packages we need to run a slapd server on RedHat

class slapd_proxy::packages::redhat {

  file { '/etc/sysconfig/ldap':
    mode    => 664,
    content => template('slapd_proxy/etc/sysconfig/ldap.erb'),
  }

  package {
    'cyrus-sasl-gssapi':
      ensure  => installed;
    'openldap-clients':
      ensure  => installed,
      require => [ File['/etc/openldap/schema'],
                   File['/etc/openldap/slapd.conf'] ];
    'openldap-servers':
      ensure  => installed,
      require => [ File['/etc/openldap/schema'],
                   File['/etc/openldap/slapd.conf'] ];
  }
}

# ------------------------------------------------------------
# Install the packages we need to run a slapd server on Debian

class slapd_proxy::packages::debian {
  include user::ldap

  file { '/etc/default/slapd':
    mode    => 644,
    content => template('slapd_proxy/etc/default/slapd.erb'),
  }

  preseed_debconf { 'preseed-slapd':
    source => 'puppet:///slapd_proxy/etc/apt/preseed-slapd',
    answer => 'slapd.*slapd/no_configuration.*boolean.*true',
  }

  package { 'slapd':
    ensure  => installed,
    require => [ Preseed_debconf['preseed-slapd'],
                 File['/etc/default/slapd'],
                 File['/etc/ldap/schema'],
                 File['/etc/ldap/slapd.conf'] ];
  }

  package {
    'libsasl2-modules-gssapi-mit':
      ensure  => installed;
    'ldap-utils':
      ensure  => installed,
      require => File['/etc/ldap/schema'];
  }
}