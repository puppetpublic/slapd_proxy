# slapd_proxy/init.pp -- slapd proxy model

class slapd_proxy {
    include base::limits

    service { 'slapd':
        ensure     => running,
        hasstatus  => false,
        status     => 'pidof slapd',
        hasrestart => true, 
        subscribe  => [ File['/etc/default/slapd'], 
                        File['/etc/ldap/schema'],
                        File['/etc/ldap/slapd.conf'],
                        File['/etc/ldap/conf.d/slapd_custom.conf'] ],
    }

    base::limits::conf { 
        'openldap-soft':
            domain => openldap,
            type   => soft,
            item   => nofile,
            value  =>  4096;
        'openldap-hard':
            domain => openldap,
            type => hard,
            item => nofile,
            value => 10240;
    }

    file {
        '/etc/ldap':
            ensure => directory;
        '/etc/ldap/conf.d': 
            ensure => directory;
        '/etc/ldap/slapd.conf': 
            source  => 'puppet:///slapd_proxy/etc/ldap/slapd.conf';
        '/etc/ldap/schema':
            source  => 'puppet:///s_ldap/schema/prod24',
            recurse => true;
    }

    slapd_proxy::config { $fqdn:
        runasuser  => 'openldap',
        runasgroup => 'openldap',
    }  

    preseed_debconf { 'preseed-slapd':
        source => 'puppet:///slapd_proxy/etc/apt/preseed-slapd',
        answer => 'slapd.*slapd/no_configuration.*boolean.*true',
    }

    package { 'slapd':
        ensure => installed,
        require => [ Preseed_debconf['preseed-slapd'],
                     File['/etc/default/slapd'],
                     File['/etc/ldap/schema'],
                     File['/etc/ldap/slapd.conf'],
                     File['/etc/ldap/conf.d/slapd_custom.conf'] ];
    }
}